<?php

session_start();

require_once 'fns.php';
require_once 'db.php';

if (!isset($_SESSION['login'])) die(header(HOME));

echo '<div class="box-shadow padding10">Order history</div><br>';

$r = DB::run('SELECT "order".id, login, "order".name, user_id, amount, date, status, "order".surname, ship_address FROM "order" INNER JOIN user ON "order".user_id = user.id') or die(ERROR);#'SELECT * FROM \'order\''
$arr = $r->fetchAll();
define('NUMROWS', count($arr));

echo '
<table>
<tr>
  <th>&nbsp;</th>
  <th>Login</th>
  <th>Name</th>
  <th>Price</th>
  <th>Ship address</th>
  <th>Sum</th>
  <th>Date</th>
</tr>';

for ($i = 0; $i < NUMROWS; $i++) {

echo '
<tr onclick="detail(\''.$arr[$i]['id'].'\')" class="detail">
  <td>'.($i+1).'.</td>
  <td>'.$arr[$i]['login'].'</td>
  <td>'.$arr[$i]['name'].'</td>
  <td>'.$arr[$i]['surname'].'</td>
  <td>'.$arr[$i]['ship_address'].'</td>
  <td>'.$arr[$i]['amount'].'$</td>
  <td>'.$arr[$i]['date'].'</td>
</tr>
<tr>
  <td colspan="7" id="detail'.$arr[$i]['id'].'"></td>
</tr>';

}

echo '</table>';
