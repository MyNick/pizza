<?php

define('HOME', 'Location: index.php');
define('ROOT', 'http://localhost:8000/');#'http://'.getenv('SERVER_NAME').'/tz/pizza/'
define('ERROR', '<div style="color: #FF0000;">Ошибка запроса, обратитесь к разработчику сайта.</div>');#use
define('EURUSD', 1.18);
define('COST_DELIVERY', 10);


function head($title, $description='', $keywords='')
{

echo '
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8">
  <title>'.$title.'</title>
  
  <link rel="shortcut icon" href="'.ROOT.'favicon.ico" type="image/x-icon">

  <link href="'.ROOT.'css/style.css" rel="stylesheet">
</head>

<body>

<div style="width: 80%;margin: 0 auto;">
<div id="wrapper">
<div id="header" class="head-box box-shadow padding20">

<div>
  <img class="fit-picture" src="'.ROOT.'img/1.jpg" alt="Logo Pizza" width="60" height="60">
  <a href="'.ROOT.'index.php" title="Hot Pizza"><img class="img_brd_0" src="'.ROOT.'img/logo.jpg" alt="Логотип" width="200" height="52"></a>
</div>

<div id="auth-form">'.(isset($_SESSION['login']) ? 'Hi, '.$_SESSION['login'].' | <a href="" onclick="logout()" id="logout" title="Logout">Logout</a>&nbsp;&nbsp;&nbsp;'.user_menu() : auth_form()).'</div>
  
<div class="cart">
  <a href="#" onclick="cart()" class="cart to_cart"><img src="'.ROOT.'img/ico-cart.svg" width="31" height="29" alt="ico-cart"></a>
  <span class="counter total_cart_amount" data-bind="text: total_cart_amount" id="card-counter">'.(isset($_SESSION['quantity']) ? array_sum($_SESSION['quantity']) : 0).'</span>
</div>

</div><!-- .header-->

</div><!-- .wrapper--><br>'."\n";

}

function auth_form()
{

return '
  <form class="-form" method="post" action="#">
  <div class="form">
    <div>&nbsp;</div><div>&nbsp;</div>
    <div><a href="">Sign In</a></div>

    <div><label>Email:</label></div><div>&nbsp;</div>
    <div><input type="text" name="login" id="login" class="width50"></div>
    <div><label>Password:</label></div><div>&nbsp;</div>
    <div><input type="password" name="password" id="password" class="width50"></div>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div id="error"></div>
    <div>&nbsp;</div>

    <div>&nbsp;</div>
    <div><input type="button" onclick="log()" value="Login"></div>
  </div>
  </form>';

}

function stat_menu()
{

return '
<ul id="menu">
  <li><div><a href="'.ROOT.'index.php" title="Hot Pizza">Home</a></div></li>
  <li><div>Item 2</div></li>
  <li><div>Item 3</div></li>
</ul>';

}

function user_menu() {

return '<div class="dropdown">
  <button class="dropbtn">User</button>
  <div class="dropdown-content">
    <a href="#" onclick="order_history()">Order history</a>
    <a href="#">Link 2</a>
    <a href="#">Link 3</a>
  </div>
</div>';

}

function content($content, $show_module_1='', $show_module_2='', $show_module_3='')
{
$r = DB::run('SELECT * FROM pizza') or die(ERROR);
$arr = $r->fetchAll();
define('NUMROWS', count($arr));

#for ($i = 0; $i < NUMROWS; $i++) #echo $arr[$i]['name']."\n";
	#echo '<div id="'.$arr[$i]['name'].'"><a href="'.ROOT.'index.php" title=""><img class="" src="'.ROOT.'img/'.$arr[$i]['img'].'.jpg" alt="'.$arr[$i]['description'].'" width="140" height="140"></a><span>'.$arr[$i]['name'].'</span></div>'."\n";
    #if ($i <= $num_parables_2) $one_list .= '<span class="numbers">'.($i+1).'</span><span class="dot">.</span> <a href="?verse_id='.$r[$i]['verse_id'].'" class="verse_name">'.$r[$i]['vn'].' - <span class="poet">'.$r[$i]['pn'].'</span></a><br />'."\n";

echo '
<div class="grid-box box-shadow">
	<div class="col-1 box-shadow">'.stat_menu().'</div>
	<div class="col-2"></div>
	<div class="col-3 box-shadow" id="ajax-box">
	    <div class="box-shadow padding10">Menu</div><br>
	    <div class="pizza">'."\n";
for ($i = 0; $i < NUMROWS; $i++) echo '<div id="id'.$arr[$i]['id'].'"><a href="#" title=""><img class="" src="'.ROOT.'img/'.$arr[$i]['img'].'.jpg" alt="'.$arr[$i]['description'].'" title="'.$arr[$i]['description'].'" width="140" height="140"></a><div id="name'.$arr[$i]['id'].'">'.$arr[$i]['name'].'</div>
<div><span id="p'.$arr[$i]['id'].'">'.$arr[$i]['price'].'</span>$ (<span id="pe'.$arr[$i]['id'].'">'.round($arr[$i]['price']/EURUSD, 2).'</span>&euro;)&nbsp;&nbsp;&nbsp;</div>

<div class="less-more">
  <a class="minus" href="#" onclick="minus(\''.$arr[$i]['id'].'\','.$arr[$i]['price'].')">- </a>
  <input type="text" class="amount" id="am'.$arr[$i]['id'].'" readonly="readonly" value="1">
  <a class="plus" href="#" onclick="plus(\''.$arr[$i]['id'].'\','.$arr[$i]['price'].')"> +</a>
</div><br>

<div class="add-to-cart"><input type="button" value="Add to cart" onclick="to_cart(\''.$arr[$i]['id'].'\')"></div><br>

</div>'."\n";
echo '</div>
	</div>
</div><br>'."\n";

}

function add()
{

	#@post_check();

	$q = 'INSERT INTO "order"
	     (id, user_id, amount, date, status, name, surname, ship_address)
          VALUES(null,
               '.(isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0).',
               "'.$_SESSION['total'].'",
               datetime(),
               "Confirmed",
               "'.$_POST['name'].'",
               "'.$_POST['surname'].'",
               "'.$_POST['ship_address'].'")';

	$r = DB::run($q) or die(ERROR);

    define('NUMROWS', count($_SESSION['name']));
	$last_id = DB::run('SELECT last_insert_rowid() as last_insert_rowid')->fetch() or die(ERROR);

    for ($i = 0; $i < NUMROWS; $i++) {
	  $q = 'INSERT INTO "order_items"
	       (id, order_id, pizza_id, item_price, quantity)
            VALUES(null,
                '.$last_id['last_insert_rowid'].',
                "'.$_SESSION['id'][$i].'",
                "'.$_SESSION['price'][$i].'",
                "'.$_SESSION['quantity'][$i].'")';

	  $r = DB::run($q) or die(ERROR);
	}
	return true;
}

function order_form()
{

return '
<div class="box-shadow padding10">Order form</div><br><br>
  <form id="survey-form" method="GET" action="">
	<div class="rowTab">
	  <label id="name-label" for="name">* Name: </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <input autofocus type="text" name="no_empty[name]" id="name" class="input-field" placeholder="Enter your name" required>
	</div>
	<div class="rowTab">
	  <label id="name-label" for="name">* Surname: </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <input autofocus type="text" name="no_empty[surname]" id="surname" class="input-field" placeholder="Enter your surname" required>
	</div>
	<div class="rowTab">
	  <label id="name-label" for="name">* Ship address: </label>
	  <input autofocus type="text" name="no_empty[ship_address]" id="ship_address" class="input-field" placeholder="Enter ship address" required>
	</div><br>
	<button id="submit" type="submit" onclick="order_go()">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<span id="order-error"></span>
  </form>';

}

function js($scripts) { echo $scripts."\n"; }

function foot()
{

echo '
<footer class="box-shadow padding20"><br>Copyright &copy; <span style="color: #f96224;text-decoration: underline;">Hot Pizza</span>, october 2020. All rights reserved.<br><br></footer>

</body>
</html>

<script src="'.ROOT.'js/jquery-3.5.1.min.js"></script>
<script>
const EURUSD = '.EURUSD.';

function minus(id,price)
{
  if(Number($("#am"+id).attr("value"))<=1) return;
  $("#p"+id).html(Number($("#p"+id).html())-price);
  $("#am"+id).attr("value", Number($("#am"+id).attr("value"))-1);
  $("#pe"+id).html(($("#p"+id).html()/EURUSD).toFixed(2));
}

function plus(id,price)
{
  if(Number($("#am"+id).attr("value"))>1000) return;
  $("#am"+id).attr("value", Number($("#am"+id).attr("value"))+1);
  $("#p"+id).html(Number($("#am"+id).attr("value"))*price);
  $("#pe"+id).html(($("#p"+id).html()/EURUSD).toFixed(2));
}

//$("#log").click(function(){//alert($("#login").val()+ " pas: "+$("#password").val());
function log() {

$.ajax({
  type:"post",
  url:"'.ROOT.'index.php",
  data:{
    login:$("#login").val(),
    password:$("#password").val()
  }
  }).done(function(response) {
    var response = JSON.parse(response);
    if (response[0] === "false") $("#error").html("Login or password incorrect");
    else { $("#auth-form").html("Hi, " + response[1] + ` | <a href="" onclick="logout()" id="logout" title="Logout">Logout</a>&nbsp;&nbsp;&nbsp;` + user_menu()); $("#card-counter").html("0"); }
    
  }).fail(function() { console.log("fail"); });

};

function user_menu() {
return `<div class="dropdown">
  <button class="dropbtn">User</button>
  <div class="dropdown-content">
    <a href="#" onclick="order_history()">Order history</a>
    <a href="#">Link 2</a>
    <a href="#">Link 3</a>
  </div>
</div>`;
}

//$("#logout").on("click", function(){
function logout() {

event.preventDefault();

$.ajax({
  type:"post",
  url:"'.ROOT.'index.php",
  data:{ logout: "true" }
  }).done(function(response) {//alert("(response) = "+response);return;
  $("#auth-form").html(`<div id="auth-form">
  <form class="-form" method="post" action="#">
  <div class="form">
    <div>&nbsp;</div><div>&nbsp;</div>
    <div><a href="" class="elementor-item">Sign In</a></div>

    <div><label>Email:</label></div><div>&nbsp;</div>
    <div><input type="text" name="login" id="login" class="width50"></div>
    <div><label>Password:</label></div><div>&nbsp;</div>
    <div><input type="password" name="password" id="password" class="width50"></div>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div id="error"></div>
    <div>&nbsp;</div>

    <div>&nbsp;</div>
    <div><input type="button" onclick="log()" value="Login"></div>
  </div>
  </form>
  </div>`);
  $("#card-counter").html("0");
  }).fail(function() { console.log("logout fail"); });

};

function order() {

event.preventDefault();

$.ajax({
  type:"post",
  url:"'.ROOT.'index.php",
  data:{ order: "true" }
  }).done(function(response) {
    //alert("answer, " + response);return;
    var response = JSON.parse(response);
    $("#ajax-box").html(response[0]);  
  }).fail(function() { console.log("fail"); });
}

function order_go() {

event.preventDefault();

$.ajax({
  type:"post",
  url:"'.ROOT.'index.php",
  data:{
     order_go: "true",
     name:$("#name").val(),
     surname:$("#surname").val(),
     ship_address:$("#ship_address").val()
  }
  }).done(function(response) {
    var response = JSON.parse(response);
    if(response[0] === "true") $("#order-error").css({fontSize: "12pt", color: "green"}).html("Your order has been successfully confirmed");
    else $("#order-error").css({fontSize: "12pt", color: "red"}).html("Error: order not saved");
  }).fail(function() { console.log("fail"); });
}

function order_history(){
  event.preventDefault();
  $("#ajax-box").load("'.ROOT.'order-history.php");  
}

function detail(id){
  if ($("#detail"+id).html().length > 0) { $("#detail"+id).html(""); return; }
  $("#detail"+id).load("'.ROOT.'order-detail.php", { id: id });//$("#detail"+id).slideToggle();  
}

function cart(){
  event.preventDefault();
  $("#ajax-box").load("'.ROOT.'cart.php");  
}

function to_cart(id) {
  event.preventDefault();
  $.ajax({
    type:"post",
    url:"'.ROOT.'index.php",
    data:{
      id:id,
      name:$("#name"+id).html(),
      price:$("#p"+id).html(),
      quantity:$("#am"+id).attr("value")
    }
    }).done(function(response) { $("#card-counter").html(JSON.parse(response)[0]);
    }).fail(function() { console.log("fail"); });
};

function show_cart() {
  $.ajax({
    type:"post",
    url:"store_items.php",
    data:{ showcart:"cart" },
    success:function(response) {
      document.getElementById("mycart").innerHTML=response;
      $("#mycart").slideToggle();
    }
  });

}
</script>
'."\n\n";

}

function login($login, &$password)
{

  #die('$q = '.$q);
  $r = DB::run('SELECT id,login,password FROM user WHERE login = \''.$login.'\'') or die(ERROR);

  $arr = $r->fetchAll();

  if (count($arr) == 1) {
	  
	  if (password_verify($password, $arr[0]['password'])) {
		  
		$_SESSION = [];
		#session_destroy();
		
		$_SESSION['user_id'] = $arr[0]['id'];  
		$_SESSION['login'] = $arr[0]['login'];
		return 1;
		
	  } else return 0;

  } else return 0;

}

function empty_fields_in_form()
{

  return in_array('', $_POST['no_empty']);

}

function form_empty()
{

  foreach ($_POST['no_empty'] as $value) {

    if ($value != '') return 0;

  }

  return 1;

}

function post_check()
{
//  if (get_magic_quotes_gpc()) foreach ($_POST['no_empty'] as &$v) $v = htmlspecialchars(trim($v), ENT_NOQUOTES);
//  else
  foreach ($_POST['no_empty'] as &$v) $v = htmlspecialchars(addslashes(trim($v)), ENT_NOQUOTES);

}

?>
