<?php

session_start();

require_once 'fns.php';
require_once 'db.php';


if(isset($_POST['order'])) { die(json_encode([order_form()])); }

if(isset($_POST['order_go'])) { if(add()) die(json_encode(['true'])); else die(json_encode(['false'])); }

if(isset($_POST['logout'])) {
  $_SESSION = [];
  #session_destroy();
  die(json_encode(['true']));
}

if(isset($_POST['login'], $_POST['password']))
{
  if(login(addslashes(trim($_POST['login'])), $_POST['password'])) {
    die(json_encode(['true', $_SESSION['login']]));
  }
  else { die(json_encode(['false'])); }
}

if(isset($_POST['name']))
{
  if(isset($_SESSION['id'])) {
    
    $index = array_search($_POST['id'], $_SESSION['id']);  
    if($index === FALSE) {
      $_SESSION['id'][]=$_POST['id'];	
      $_SESSION['name'][]=$_POST['name'];
      $_SESSION['price'][]=$_POST['price'];
      $_SESSION['quantity'][]=$_POST['quantity'];
    }
    else { $_SESSION['quantity'][$index]=$_SESSION['quantity'][$index]+$_POST['quantity']; }
  }
  else {
    $_SESSION['id'][]=$_POST['id'];	
    $_SESSION['name'][]=$_POST['name'];
    $_SESSION['price'][]=$_POST['price'];
    $_SESSION['quantity'][]=$_POST['quantity'];
  }

  die(json_encode([array_sum($_SESSION['quantity'])]));
}

head('The pizza task','The pizza task','pizza');

content('');

foot();
