<?php

session_start();

require_once 'fns.php';
require_once 'db.php';

if (!isset($_SESSION['login'])) die(header(HOME));

$r = DB::run('SELECT "order_items".id, name, item_price, quantity FROM order_items INNER JOIN pizza ON order_items.pizza_id = pizza.id WHERE order_id='.$_POST['id'].'') or die(ERROR);#'SELECT id, pizza_id, item_price, quantity FROM \'order_items\' WHERE order_id='.$_POST['id'].''
$arr = $r->fetchAll();
define('NUMROWS', count($arr));

echo '
<div class="cart-table">
  <div class="cart-tr">
    <div class="cart-th">&nbsp;</div><div class="cart-th">Pizza name</div><div class="cart-th">Price</div><div class="cart-th">Quantity</div>
  </div>';

for ($i = 0; $i < NUMROWS; $i++) {

echo '
<div class="cart-tr">
  <div class="cart-td-0">'.($i+1).'.</div>
  <div class="cart-td-1">'.$arr[$i]['name'].'</div>
  <div class="cart-td-2">'.$arr[$i]['item_price'].'$</div>
  <div class="cart-td-3">'.$arr[$i]['quantity'].'</div>
</div>';

}

echo '</div>';
