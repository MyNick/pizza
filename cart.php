<?php

session_start();

require_once 'fns.php';
require_once 'db.php';


echo '<div class="box-shadow padding10">Cart</div><br>';

if(isset($_SESSION['name']) && count($_SESSION['name']) > 0) define('NUMROWS', count($_SESSION['name']));
else die('<div class="center"><br><br>Cart is empty<br><br><br></div>');


echo '<div class="cart-table">
  <div class="cart-tr">
    <div class="cart-th">&nbsp;</div><div class="cart-th">Name</div><div class="cart-th">Price</div><div class="cart-th">Quantity</div><div class="cart-th">Sum</div>
  </div>';

$_SESSION['total'] = 0;

for ($i = 0; $i < NUMROWS; $i++) {
  $sum = $_SESSION['price'][$i]*$_SESSION['quantity'][$i];
  $_SESSION['total'] += $sum;
echo '<div class="cart-tr">
    <div class="cart-td-0">'.($i+1).'.</div>
    <div class="cart-td-1">'.$_SESSION['name'][$i].'</div>
    <div class="cart-td-2">'.$_SESSION['price'][$i].'$</div>
    <div class="cart-td-3">'.$_SESSION['quantity'][$i].'</div>
    <div class="cart-td-4">'.$sum.'$</div>
  </div>';

}

echo '</div>';

echo '<br><br><div class="order"><span style="text-align:left; color:#a2a2a2">Total: <strong>'.$_SESSION['total'].'$ (<span id="">'.round($_SESSION['total']/EURUSD, 2).'</span>&euro;) +'.COST_DELIVERY.'$ delivery</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="order" onclick="order();">Place an order >></a></div><br><br>'."\n";

#foot();
